package com.example.starter.webmvc.util;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.lang.Opt;
import com.example.starter.util.AssertUtil;
import lombok.experimental.UtilityClass;

import javax.validation.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author 王令
 * @since 2022/11/23 9:21
 */
@UtilityClass
public class ValidationUtil {

    private static final ValidatorFactory VALIDATOR_FACTORY = Validation.buildDefaultValidatorFactory();

    public void batchValidate(Collection<?> batch, Class<?>... groups) {
        Validator validator = VALIDATOR_FACTORY.getValidator();
        Set<ConstraintViolation<Object>> errResult = new HashSet<>();
        Opt.ofEmptyAble(batch).orElse(ListUtil.empty()).forEach(v -> errResult.addAll(validator.validate(v, groups)));

        AssertUtil.isEmpty(errResult, () -> new ConstraintViolationException(errResult));
    }

}
