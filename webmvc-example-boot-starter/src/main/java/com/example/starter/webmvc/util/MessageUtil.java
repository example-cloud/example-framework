package com.example.starter.webmvc.util;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.extra.spring.SpringUtil;
import lombok.experimental.UtilityClass;
import org.springframework.context.MessageSource;

import java.util.Locale;

/**
 * <p>
 * message工具
 * </p>
 *
 * @author 王令
 * @since 2022-05-13 21-12-12
 */
@UtilityClass
public class MessageUtil {

    public String getMessage(String code) {
        return getMessage(code, Locale.CHINA);
    }

    public String getMessage(String code, Locale locale) {
        return getMessage(code, locale, ListUtil.empty());
    }

    public String getMessage(String code, Locale locale, Object... objects) {
        MessageSource messageSource = SpringUtil.getBean(MessageSource.class);
        return messageSource.getMessage(code, objects, locale);
    }
}
