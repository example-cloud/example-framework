package com.example.starter.slf4j.config;

import com.example.starter.slf4j.log.*;
import com.example.starter.slf4j.support.LogContext;
import org.slf4j.Logger;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * 日志自动装配
 * @author xsx
 * @date 2019/6/19
 * @since 1.8
 */
@Configuration
@EnableConfigurationProperties(Slf4jProperties.class)
@ConditionalOnClass({Logger.class})
@Import(value = {
        LogContext.class,
        DefaultLogFormatter.class,
        DefaultParamLogFormatter.class,
        DefaultResultLogFormatter.class,
        VoidLogCallback.class
})
public class LogAutoConfiguration {
    @Bean
    public LogProcessor logProcessor() {
        return new LogProcessor();
    }
}
