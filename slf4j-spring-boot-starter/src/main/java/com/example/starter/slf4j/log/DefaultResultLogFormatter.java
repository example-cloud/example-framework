package com.example.starter.slf4j.log;

import org.slf4j.Logger;
import com.example.starter.slf4j.support.LogHandler;
import com.example.starter.slf4j.support.MethodInfo;

/**
 * 默认结果日志格式化实现
 * @author xsx
 * @date 2020/5/7
 * @since 1.8
 */
public class DefaultResultLogFormatter implements ResultLogFormatter {

    /**
     * 默认实现名称
     */
    public static final String DEFAULT = DefaultResultLogFormatter.class.getName();

    /**
     * 格式化
     * @param log 日志对象
     * @param level 日志级别
     * @param busName 业务名称
     * @param methodInfo 方法信息
     * @param result 返回结果
     */
    @Override
    public void format(
            Logger log,
            Level level,
            String busName,
            MethodInfo methodInfo,
            Object result
    ) {
        LogHandler.print(log, level, LogHandler.getAfterInfo(busName, methodInfo, result));
    }
}
