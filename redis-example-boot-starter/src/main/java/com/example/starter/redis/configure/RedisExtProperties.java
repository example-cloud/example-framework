package com.example.starter.redis.configure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 王令
 * @since 2023/10/16
 */
@Data
@ConfigurationProperties(prefix = "spring.redis")
public class RedisExtProperties {

    // 集群配型 可选值：standalone, sentinel, cluster
    private String clusterType = "standalone";
}
