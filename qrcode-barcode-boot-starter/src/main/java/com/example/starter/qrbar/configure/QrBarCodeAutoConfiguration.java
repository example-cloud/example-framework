package com.example.starter.qrbar.configure;

import com.example.starter.qrbar.barcode.BarcodeGenerator;
import org.iherus.codegen.qrcode.QrcodeGenerator;
import org.iherus.codegen.qrcode.SimpleQrcodeGenerator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 王令
 * @since 2023/11/24 10:16
 */
@Configuration
public class QrBarCodeAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(QrcodeGenerator.class)
    public QrcodeGenerator qrcodeGenerator() {
        return new SimpleQrcodeGenerator();
    }

    @Bean
    @ConditionalOnMissingBean(BarcodeGenerator.class)
    public BarcodeGenerator barcodeGenerator() {
        return new BarcodeGenerator();
    }
}
