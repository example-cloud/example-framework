package com.example.starter.qrbar.barcode;

import com.google.zxing.WriterException;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author 王令
 * @since 2023/11/24 10:18
 */
public interface Generator {

    Generator width(int width);

    Generator height(int height);

    Generator generate(String text) throws WriterException;

    void toStream(OutputStream os) throws IOException;

    void clear();
}
