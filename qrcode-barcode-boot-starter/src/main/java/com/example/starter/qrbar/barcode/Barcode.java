package com.example.starter.qrbar.barcode;

import com.google.zxing.common.BitMatrix;
import lombok.Data;

/**
 * @author 王令
 * @since 2023/11/24 10:23
 */
@Data
public class Barcode {

    private BitMatrix matrix;

    private int width = 300;

    private int height = 100;
}
