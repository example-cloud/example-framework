package com.example.starter.redisson.constant;

/**
 * @author 王令
 * @since 2022/11/14 12:56
 */
public enum LockMode {

    // 可重入锁
    REENTRANT_LOCK,
    // 公平锁
    FAIR_LOCK,
    // 读锁
    READ_LOCK,
    // 写锁
    WRITE_LOCK
}
