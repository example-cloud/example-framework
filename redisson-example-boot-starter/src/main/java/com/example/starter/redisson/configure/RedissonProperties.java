package com.example.starter.redisson.configure;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 王令
 * @since 2022/11/30 10:40
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "com.example.redisson")
public class RedissonProperties {

    private String lockPrefix = "e:lock:";
}
