package com.example.starter.redisson.handler;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.example.starter.redisson.anntation.RedissonLock;

/**
 * @author 王令
 * @since 2022/11/23 15:29
 */
public class KeyGeneratorStrategyFactory {

    public static KeyGenerator election(RedissonLock lock) {
        return SpringUtil.getBeansOfType(KeyGenerator.class).values().stream()
                .filter(keyGenerator -> keyGenerator.getClass().equals(
                        StrUtil.isBlank(lock.key())?
                                DefaultKeyGenerator.class:
                                lock.keyGenerator()
                )).findFirst().orElse(null);
    }
}
