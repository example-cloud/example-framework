package com.example.starter.redisson.handler;

import cn.hutool.core.text.StrPool;
import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.StrUtil;
import com.example.starter.redisson.anntation.RedissonLock;
import com.example.starter.redisson.configure.RedissonProperties;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.JoinPoint;

/**
 * @author 王令
 * @since 2022/11/23 15:04
 */
@RequiredArgsConstructor
public class DefaultKeyGenerator implements KeyGenerator {

    private final RedissonProperties properties;

    @Override
    public String generate(JoinPoint point, RedissonLock lock) {
        if (ObjUtil.isNotEmpty(point.getArgs())) {
            return properties.getLockPrefix()
                    + lock.key()
                    + (StrUtil.isBlank(lock.name()) ? point.getSignature().getName() : lock.name())
                    + StrPool.COLON
                    + StrUtil.join(StrPool.COLON, point.getArgs());
        }
        return properties.getLockPrefix()
                + lock.key()
                + (StrUtil.isBlank(lock.name()) ? point.getSignature().getName() : lock.name());
    }
}
