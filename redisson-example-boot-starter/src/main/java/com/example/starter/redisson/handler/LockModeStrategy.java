package com.example.starter.redisson.handler;

import com.example.starter.redisson.constant.LockMode;
import org.redisson.api.RLock;

/**
 * @author 王令
 * @since 2022/11/23 14:22
 */
public interface LockModeStrategy {

    /**
     * 是否被选中
     */
    boolean elected(LockMode mode);

    /**
     * 获取RLock
     */
    RLock getLock(String key);
}
