package com.example.starter.mybatis.configure;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 王令
 * @since 2022/10/3 8:45
 */
@Setter
@Getter
@ConfigurationProperties("com.example.mybatis")
public class MybatisProperties {

    private MultiTenant multiTenant = new MultiTenant();

    @Getter
    @Setter
    public static class MultiTenant {
        private Boolean enabled = true;
        private String basePackage;
    }

}
