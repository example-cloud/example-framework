package com.example.starter.job.configure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 王令
 * @since 2023/8/2
 */
@Data
@ConfigurationProperties(prefix = "com.example.xxl-job.admin")
public class XxlJobAdminProperties {

    private String addresses = "http://127.0.0.1:2210/xxl-job-admin";

}
