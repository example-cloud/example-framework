package com.example.starter.job.configure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 王令
 * @since 2023/8/2
 */
@Data
@ConfigurationProperties(prefix = "com.example.xxl-job.executor")
public class XxlJobExecutorProperties {

    private String appname = "xxl-job-executor-sample";

    private String address = "";

    private String ip = "";

    private int port = 9999;

    private String logPath = "data/applogs/xxl-job/jobhandler";

    private int logRetentionDays = 30;
}
