package com.example.starter.job.configure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 王令
 * @since 2023/8/2
 */
@Data
@ConfigurationProperties(prefix = "com.example.xxl-job")
public class XxlJobProperties {

    private Boolean enabled = true;

    private String accessToken = "default_token";

}
