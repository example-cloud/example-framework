package com.example.starter.feign.configure;

import com.example.starter.feign.interceptor.ExampleRequestInterceptor;
import feign.RequestInterceptor;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 王令
 * @since 2022/9/5 21:43
 */
@Configuration
@RequiredArgsConstructor
public class FeignAutoConfiguration {

    private final HttpServletRequest request;

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public RequestInterceptor requestInterceptor() {
        return new ExampleRequestInterceptor(request);
    }
}
