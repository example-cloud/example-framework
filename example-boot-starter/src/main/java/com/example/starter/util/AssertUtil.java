package com.example.starter.util;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;

import java.util.Collection;
import java.util.function.Supplier;

/**
 * @author 王令
 * @since 2024/9/8
 */
public class AssertUtil extends Assert {

    public static <E, T extends Collection<E>, X extends Throwable> T isEmpty(T collection, Supplier<X> errorSupplier) throws X {
        if (CollUtil.isNotEmpty(collection)) {
            throw errorSupplier.get();
        }
        return collection;
    }

    public static <T> T contains(Collection<T> collection, T value, String errorMsgTemplate, Object... params) throws IllegalArgumentException {
        return contains(collection, value, () -> new IllegalArgumentException(StrUtil.format(errorMsgTemplate, params)));
    }

    public static <T, X extends Throwable> T contains(Collection<T> collection, T value, Supplier<? extends X> errorSupplier) throws X {
        if (!CollUtil.contains(collection, value)) {
            throw errorSupplier.get();
        }
        return value;
    }
}
