package com.example.starter.util;

import com.example.starter.exception.BusinessException;
import lombok.experimental.UtilityClass;
import org.springframework.http.HttpStatus;

/**
 * @author 王令
 * @since 2022/12/30 11:28
 */
@UtilityClass
public class ExceptionUtil {

    public BusinessException wrap() {
        return new BusinessException();
    }

    public BusinessException wrap(String message) {
        return new BusinessException(message);
    }

    public BusinessException wrap(String message, HttpStatus status) {
        return new BusinessException(message, status);
    }

    public BusinessException wrapNotFound() {
        return new BusinessException(HttpStatus.NOT_FOUND);
    }

    public BusinessException wrapNotFound(String message) {
        return new BusinessException(message, HttpStatus.NOT_FOUND);
    }

    public BusinessException wrapForbidden() {
        return new BusinessException(HttpStatus.FORBIDDEN);
    }

    public BusinessException wrapForbidden(String message) {
        return new BusinessException(message, HttpStatus.FORBIDDEN);
    }
}
