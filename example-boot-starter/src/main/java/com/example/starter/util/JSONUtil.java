package com.example.starter.util;

import cn.hutool.core.lang.Opt;
import com.example.starter.jackson.module.JavaTimeModule;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.SneakyThrows;


/**
 * @author 王令
 * @since 2022/9/19 8:41
 */
public class JSONUtil {

    @Getter
    private static final ObjectMapper instance;

    static {
        instance = new ObjectMapper();
        final JavaTimeModule javaTimeModule = new JavaTimeModule();
        instance.registerModule(javaTimeModule);
    }

    @SneakyThrows
    public static String toJsonStr(Object o) {
        return instance.writeValueAsString(o);
    }

    @SneakyThrows
    public static byte[] toJsonByte(Object o) {
        return instance.writeValueAsBytes(o);
    }

    public static <T> T parse(byte[] bytes, Class<T> clazz) {
        return Opt.ofTry(() -> instance.readValue(bytes, clazz)).orElse(null);
    }

    public static JsonNode parse(String json) {
        return Opt.ofTry(() -> instance.readTree(json)).orElse(null);
    }

    public static <T> T parse(String json, Class<T> clazz) {
        return Opt.ofTry(() -> instance.readValue(json, clazz)).orElse(null);
    }

    public static <T> T parse(String json, TypeReference<T> typeReference) {
        return Opt.ofTry(() -> instance.readValue(json, typeReference)).orElse(null);
    }

}
