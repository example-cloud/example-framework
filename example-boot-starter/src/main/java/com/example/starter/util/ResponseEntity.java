package com.example.starter.util;

import com.example.starter.constant.ResponseStatus;
import com.example.starter.exception.BusinessException;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * @author 王令
 * @since 2024/5/7
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseEntity<T> {

    private int code;
    private String message;
    private T data;
    private Long timestamp;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String traceId;

    public static <T> ResponseEntity<T> ok() {
        return ResponseEntity
                .<T>builder()
                .code(HttpStatus.OK.value())
                .message(HttpStatus.OK.getReasonPhrase())
                .build();
    }

    public static <T> ResponseEntity<T> ok(T data) {
        return ResponseEntity
                .<T>builder()
                .code(HttpStatus.OK.value())
                .message(HttpStatus.OK.getReasonPhrase())
                .data(data)
                .build();
    }

    public static <T> ResponseEntity<T> fail() {
        return ResponseEntity
                .<T>builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .message(HttpStatus.BAD_REQUEST.getReasonPhrase())
                .build();
    }

    public static <T> ResponseEntity<T> fail(RuntimeException e) {
        return ResponseEntity
                .<T>builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .message(e.getLocalizedMessage())
                .build();
    }


    public static <T> ResponseEntity<T> fail(BusinessException e) {
        return ResponseEntity
                .<T>builder()
                .code(e.getCode())
                .message(e.getLocalizedMessage())
                .build();
    }

    public static <T> ResponseEntity<T> fail(String message) {
        return ResponseEntity
                .<T>builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .message(message)
                .build();
    }

    public static <T> ResponseEntity<T> fail(HttpStatus status) {
        return ResponseEntity
                .<T>builder()
                .code(status.value())
                .message(status.getReasonPhrase())
                .build();
    }

    public static <T> ResponseEntity<T> fail(ResponseStatus status) {
        return ResponseEntity
                .<T>builder()
                .code(status.getCode())
                .message(status.getMessage())
                .build();
    }

    public static <T> ResponseEntity<T> fail(String message, T data) {
        return ResponseEntity
                .<T>builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .message(message)
                .data(data)
                .build();
    }

    public static <T> ResponseEntity<T> error() {
        return ResponseEntity
                .<T>builder()
                .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
                .build();
    }

    public static <T> ResponseEntity<T> error(String message) {
        return ResponseEntity
                .<T>builder()
                .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message(message)
                .build();
    }

    public static <T> ResponseEntity<T> error(HttpStatus status) {
        return ResponseEntity
                .<T>builder()
                .code(status.value())
                .message(status.getReasonPhrase())
                .build();
    }

    public static <T> ResponseEntity<T> error(ResponseStatus status) {
        return ResponseEntity
                .<T>builder()
                .code(status.getCode())
                .message(status.getMessage())
                .build();
    }

}
