package com.example.starter.constant;

import org.springframework.http.HttpStatus;

/**
 * <p>
 * 响应状态接口
 * </p>
 *
 * @author 王令
 * @since 2021-12-30
 */
public interface ResponseStatus {

    Integer OK_CODE = 200;
    String OK_MSG = HttpStatus.OK.getReasonPhrase();
    Integer FAIL_CODE = 400;
    String FAIL_MSG = HttpStatus.BAD_REQUEST.getReasonPhrase();
    Integer ERROR_CODE = 500;
    String ERROR_MSG = HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();


    /**
     * 响应码
     *
     * @return 响应码
     */
    Integer getCode();

    /**
     * 响应消息
     *
     * @return 响应消息
     */
    String getMessage();
}
