package com.example.starter.constant;

/**
 * @author 王令
 * @since 2023/1/15
 */
public interface Constants {

    String AUTHORIZATION = "Authorization";

    String TOKEN = "token";

    /**
     * 请求来源
     */
    String FROM = "From";

    /**
     * 内部feign请求标记
     */
    String FROM_INNER = "Inner";

    String TENANT = "tenantId";

    String APPLICATION_JSON_VALUE_UTF_8 = "application/json;charset=UTF-8";

}
