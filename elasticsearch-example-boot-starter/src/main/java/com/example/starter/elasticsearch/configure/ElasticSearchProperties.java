package com.example.starter.elasticsearch.configure;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 王令
 * @since 2023/9/14 14:45
 */
@Setter
@Getter
@ConfigurationProperties("com.example.elasticsearch")
public class ElasticSearchProperties {

    private String hostAndPort = "127.0.0.1:9200";
}
