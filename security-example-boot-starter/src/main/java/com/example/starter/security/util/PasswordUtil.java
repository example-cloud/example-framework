package com.example.starter.security.util;

import cn.hutool.extra.spring.SpringUtil;
import lombok.experimental.UtilityClass;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

/**
 * <p>
 * 密码编码校验工具
 * </p>
 *
 * @author 王令
 * @since 2022/7/16 12:37
 */
@UtilityClass
public class PasswordUtil {

    public Optional<PasswordEncoder> passwordEncoder() {
        return Optional.ofNullable(SpringUtil.getBean(PasswordEncoder.class));
    }

    public String encode(CharSequence rawPassword) {
        return passwordEncoder().map(passwordEncoder -> passwordEncoder.encode(rawPassword)).orElse(null);
    }

    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return passwordEncoder().map(passwordEncoder -> passwordEncoder.matches(rawPassword, encodedPassword)).orElse(false);
    }

    public boolean upgradeEncoding(String encodedPassword) {
        return passwordEncoder().map(passwordEncoder -> passwordEncoder.upgradeEncoding(encodedPassword)).orElse(false);
    }
}
