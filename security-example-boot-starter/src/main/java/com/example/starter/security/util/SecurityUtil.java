package com.example.starter.security.util;

import com.example.starter.security.pojo.ExampleUser;
import com.example.starter.webmvc.util.WebUtil;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

/**
 * @author 王令
 * @since 2022/8/9 22:46
 */
@UtilityClass
public class SecurityUtil {

    /**
     * 获取Authentication
     */
    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public String getUsername() {
        return Optional.ofNullable(getUser()).map(ExampleUser::getUsername).orElse(null);
    }

    /**
     * 当前登录用户
     */
    public ExampleUser getUser() {
        return Optional.ofNullable(getAuthentication())
                .filter(Authentication::isAuthenticated)
                .map(Authentication::getPrincipal)
                .filter(principal -> principal instanceof ExampleUser)
                .map(principal -> {
                    ExampleUser user = (ExampleUser) principal;
                    user.setTenantId(WebUtil.getTenantId());
                    user.setPassword(null);
                    return user;
                })
                .orElse(null);
    }
}
