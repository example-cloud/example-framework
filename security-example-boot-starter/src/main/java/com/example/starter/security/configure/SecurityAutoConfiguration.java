package com.example.starter.security.configure;

import com.example.starter.security.component.ExampleRedisTokenStore;
import com.example.starter.security.handler.SecurityExceptionHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.TokenStore;

/**
 * @author 王令
 * @since 2023/2/14 13:25
 */
@RequiredArgsConstructor
@ImportAutoConfiguration({
        SecurityExceptionHandler.class,
        AuthUserWebMvcConfigurer.class,
})
@EnableConfigurationProperties(SecurityProperties.class)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityAutoConfiguration {

    private final SecurityProperties properties;

    @Bean
    public TokenStore tokenStore(RedisConnectionFactory factory) {
        ExampleRedisTokenStore tokenStore = new ExampleRedisTokenStore(factory);
        tokenStore.setPrefix(properties.getPrefix());
        return tokenStore;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
