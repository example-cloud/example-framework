package com.example.starter.security.constant;

import com.example.starter.constant.ResponseStatus;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author 王令
 * @since 2022/9/13 17:27
 */
@RequiredArgsConstructor
@Getter
public enum SecurityExceptionEnum implements ResponseStatus {
    USERNAME_NOT_FOUND(401, "用户不存在"),
    INVALID_GRANT(401, "认证失败"),
    INVALID_TOKEN(401, "令牌无效"),
    ACCESS_DENIED(403, "权限不足"),
    INTERNAL_AUTHENTICATION_ERROR(503, "服务异常，无法处理身份验证请求"),
    ;

    private final Integer code;
    private final String message;
}
