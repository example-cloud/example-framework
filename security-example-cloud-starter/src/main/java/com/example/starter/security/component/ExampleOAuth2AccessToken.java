package com.example.starter.security.component;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import java.util.Date;

/**
 * @author 王令
 * @since 2023/8/7 16:41
 */
@Setter
@Getter
public class ExampleOAuth2AccessToken extends DefaultOAuth2AccessToken {

	private Date refreshExpiration;

	public ExampleOAuth2AccessToken(String value) {
		super(value);
	}

	public ExampleOAuth2AccessToken(OAuth2AccessToken accessToken) {
		super(accessToken);
	}
}
