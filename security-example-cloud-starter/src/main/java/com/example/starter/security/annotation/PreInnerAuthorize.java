package com.example.starter.security.annotation;

import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 内部鉴权，只允许内部服务访问。外部无权访问
 * @author 王令
 * @since 2023/2/3 15:38
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE })
@PreAuthorize("@pm.inner")
public @interface PreInnerAuthorize {

}
